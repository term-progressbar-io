# -*- mode: cperl; -*-
use Test::More;

use_ok('Term::ProgressBar::IO');

use IO::File;

my $fh = IO::File->new('t/random_file','r') or
    die "Unable to open t/random_file for reading: $!";

my $pb = Term::ProgressBar::IO->new($fh);

ok($pb->target() == 9*2+3,'Correct number of bytes in __DATA__');

while (<$fh>) {
    $pb->update();
}

print STDERR $pb->last_update();
ok($pb->last_update() == $pb->target(),'Last position is now target');

close($fh);

done_testing();
