#!perl -T
use 5.006;
use strict;
use warnings FATAL => 'all';
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Term::ProgressBar::IO' ) || print "Bail out!\n";
}

diag( "Testing Term::ProgressBar::IO $Term::ProgressBar::IO::VERSION, Perl $], $^X" );
